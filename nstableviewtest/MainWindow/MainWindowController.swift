//
//  MainWindowController.swift
//  nstableviewtest
//
//  Created by Denis Dzyubenko on 06/08/2020.
//  Copyright © 2020 Denis Dzyubenko. All rights reserved.
//

import Cocoa

class MainWindowController: NSWindowController {
    init() {
        let viewController = MainViewController()
        let window = NSWindow(contentViewController: viewController)
        window.toolbar = MainWindowToolbar()
        window.title = "NSTableView example app"

        // set initial window size
        window.setContentSize(NSSize(width: 800, height: 600))

        super.init(window: window)

        // Allow saving window position and size in UserDefaults
        windowFrameAutosaveName = "Main Window"
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

