//
//  MainViewController.swift
//  nstableviewtest
//
//  Created by Denis Dzyubenko on 06/08/2020.
//  Copyright © 2020 Denis Dzyubenko. All rights reserved.
//

import Cocoa

class MainViewController: NSViewController {
    lazy var column: NSTableColumn = {
        let column = NSTableColumn(identifier: NSUserInterfaceItemIdentifier("column"))
        column.resizingMask = [.autoresizingMask]
        return column
    }()

    lazy var tableView: NSTableView = {
        let tableView = NSTableView()

        tableView.wantsLayer = true

        tableView.selectionHighlightStyle = .regular
        tableView.allowsMultipleSelection = true

        tableView.autosaveTableColumns = false

        tableView.rowSizeStyle = .custom
        tableView.usesAutomaticRowHeights = true
        tableView.columnAutoresizingStyle = .uniformColumnAutoresizingStyle
        tableView.allowsColumnReordering = false
        tableView.allowsColumnResizing = false
        tableView.intercellSpacing = .zero

        tableView.floatsGroupRows = false
        tableView.headerView = nil

        tableView.addTableColumn(column)

        tableView.dataSource = self
        tableView.delegate = self

        return tableView
    }()

    lazy var scrollView: NSScrollView = {
        let scrollView = NSScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.wantsLayer = true
        scrollView.borderType = .bezelBorder

        scrollView.documentView = tableView

        scrollView.hasVerticalScroller = true

        return scrollView
    }()

    lazy var addButton: NSButton = {
        let button = NSButton()

        button.bezelStyle = .rounded

        button.title = "Add"
        button.toolTip = "Command-click to add a bunch of items"

        button.target = self
        button.action = #selector(addButtonClicked)

        return button
    }()

    lazy var removeButton: NSButton = {
        let button = NSButton()

        button.bezelStyle = .rounded

        button.title = "Remove"
        button.isEnabled = false

        button.target = self
        button.action = #selector(removeButtonClicked)

        return button
    }()

    lazy var toggleHeightButton: NSButton = {
        let button = NSButton()

        button.bezelStyle = .rounded

        button.title = "Toggle first row height"

        button.target = self
        button.action = #selector(toggleHeightButtonClicked)

        return button
    }()


    lazy var buttonStackView: NSStackView = {
        let stackView = NSStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.orientation = .horizontal
        stackView.addArrangedSubview(addButton)
        stackView.addArrangedSubview(removeButton)
        stackView.addArrangedSubview(toggleHeightButton)
        return stackView
    }()

    struct Element {
        let text: String
        var isExpanded: Bool
    }
    var elements: [Element] = [
        .init(text: "Maecenas rutrum imperdiet erat, ac.", isExpanded: false),
        .init(text: "Proin ultricies libero at magna aliquet sodales. Donec vel lectus vitae nisl laoreet vehicula ut eu arcu. Praesent vestibulum tellus.", isExpanded: false),
    ]

    let randomAdditions = [
        "Duis vitae ex ultricies, dictum risus at, placerat felis. Maecenas eget enim sed metus gravida consectetur. Sed interdum, turpis sit.",
        "Vivamus bibendum eros nec leo posuere tincidunt sit amet non sapien. Integer placerat erat at.",
        "Maecenas tempus ex tellus, nec tempor metus feugiat nec. Sed.",
        "Nam vel risus enim. Pellentesque.",
        "Ut tempor lectus non felis dapibus consequat. Curabitur in lorem vitae ligula rutrum placerat. Mauris in metus elit. Cras ac porta mauris, sit amet iaculis tortor. Pellentesque vulputate felis purus, ac lobortis nisl efficitur id. Suspendisse vitae neque tempus neque.",
        "Proin a diam sit amet nisl finibus ultrices. Morbi risus odio, viverra vitae lectus condimentum, condimentum fringilla massa. Ut ipsum tortor, faucibus ac velit vitae, fringilla molestie ipsum. Nulla quis."
    ]
    var lastRandomAddition = -1

    override func loadView() {
        view = NSView()
        view.wantsLayer = true

        view.addSubview(scrollView)
        view.addSubview(buttonStackView)

        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8),
            scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 8),

            buttonStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8),
            buttonStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8),
            buttonStackView.topAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 8),
            buttonStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8),
        ])
    }

    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }

    @objc func addButtonClicked() {
        let numberOfItemsToAdd: Int
        if NSEvent.modifierFlags.contains(.command) {
            numberOfItemsToAdd = 100
        } else {
            numberOfItemsToAdd = 1
        }

        tableView.beginUpdates()
        for _ in 0..<numberOfItemsToAdd {
            lastRandomAddition = (lastRandomAddition + 1) % randomAdditions.count
            let newItem = randomAdditions[lastRandomAddition]

            let insertionIndex = elements.count
            elements.append(Element(text: newItem, isExpanded: false))
            tableView.insertRows(at: IndexSet(integer: insertionIndex), withAnimation: .effectGap)
        }
        tableView.endUpdates()
    }

    @objc func removeButtonClicked() {
        let selectedIndexes = tableView.selectedRowIndexes

        for index in selectedIndexes.sorted().reversed() {
            elements.remove(at: index)
        }

        tableView.removeRows(at: selectedIndexes, withAnimation: .effectFade)
    }

    @objc func toggleHeightButtonClicked() {
        let newExpandedValue = !elements[0].isExpanded
        elements[0].isExpanded = newExpandedValue
        if let view = tableView.view(atColumn: 0, row: 0, makeIfNecessary: false) as? TableCellView {
            tableView.beginUpdates()
            NSAnimationContext.runAnimationGroup { context in
                context.duration = 0.3
                context.allowsImplicitAnimation = true
                view.isExpanded = newExpandedValue
                tableView.layoutSubtreeIfNeeded()
                tableView.noteHeightOfRows(withIndexesChanged: IndexSet(integer: 0))
            }
            tableView.endUpdates()
        }
    }
}

// MARK: - NSTableView DataSource

extension MainViewController: NSTableViewDataSource {
    func numberOfRows(in tableView: NSTableView) -> Int {
        return elements.count
    }
}

// MARK: - NSTableView Delegate

extension MainViewController: NSTableViewDelegate {
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let view: TableCellView

        if let dequeuedView = tableView.makeView(
            withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "TableCellView"),
            owner: self) as? TableCellView {
            // dequeue reusable cell if available
            view = dequeuedView
        } else {
            // make new view. It will be automatically registered for dequeueing in the future
            // as long as it has "identifier" property set.
            view = TableCellView()
        }

        let element = elements[row]

        view.myTextField.stringValue = element.text
        view.isExpanded = element.isExpanded

        view.setWidth(tableColumn!.width)

        return view
    }

    func tableViewColumnDidResize(_ notification: Notification) {
        let column = notification.userInfo!["NSTableColumn"] as! NSTableColumn
        let rows = tableView.rows(in: tableView.visibleRect)

        for rowIndex in rows.lowerBound..<rows.upperBound {
            let row = tableView.rowView(atRow: rowIndex, makeIfNecessary: true)
            guard let cell = row?.view(atColumn: 0) as? TableCellView else {
                assertionFailure()
                return
            }
            cell.setWidth(column.width)
        }
    }

    func tableViewSelectionDidChange(_ notification: Notification) {
        let hasSelection = tableView.selectedRow != -1
        removeButton.isEnabled = hasSelection
    }
}
