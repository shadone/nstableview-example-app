//
//  TableCellView.swift
//  nstableviewtest
//
//  Created by Denis Dzyubenko on 06/08/2020.
//  Copyright © 2020 Denis Dzyubenko. All rights reserved.
//

import Cocoa

class TableCellView: NSTableCellView {
    static let identifier = NSUserInterfaceItemIdentifier(rawValue: "TableCellView")

    // MARK: Public

    func setWidth(_ width: CGFloat) {
        widthConstrait?.constant = width
    }

    var isExpanded: Bool = false {
        didSet {
            imageHeightConstraint?.constant = isExpanded ? expandedHeight : collapsedHeight
        }
    }

    // MARK: Private

    lazy var myImageView: NSView = {
        let view = NSView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.wantsLayer = true
        view.layer?.backgroundColor = NSColor.blue.cgColor
        return view
    }()

    lazy var myTextField: NSTextField = {
        let textField = NSTextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .clear
        textField.isEditable = false
        textField.isBordered = false
        return textField
    }()

    private var widthConstrait: NSLayoutConstraint?
    private var imageHeightConstraint: NSLayoutConstraint?

    let expandedHeight: CGFloat = 100
    let collapsedHeight: CGFloat = 70

    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)

        self.identifier = Self.identifier

        widthConstrait = widthAnchor.constraint(equalToConstant: frameRect.width)
        widthConstrait?.isActive = true

        let imageHeightConstraint = myImageView.heightAnchor.constraint(equalToConstant: collapsedHeight)
        self.imageHeightConstraint = imageHeightConstraint

        addSubview(myImageView)
        addSubview(myTextField)

        NSLayoutConstraint.activate([
            myImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            myImageView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            myImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            myImageView.widthAnchor.constraint(equalToConstant: 70),
            imageHeightConstraint,

            myTextField.leadingAnchor.constraint(equalTo: myImageView.trailingAnchor, constant: 8),
            myTextField.trailingAnchor.constraint(equalTo: trailingAnchor),
            myTextField.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            myTextField.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
