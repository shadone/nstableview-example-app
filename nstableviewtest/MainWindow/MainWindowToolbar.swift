//
//  MainWindowToolbar.swift
//  nstableviewtest
//
//  Created by Denis Dzyubenko on 06/08/2020.
//  Copyright © 2020 Denis Dzyubenko. All rights reserved.
//

import Cocoa

class MainWindowToolbar: NSToolbar {
    init() {
        super.init(identifier: .init("MainToolbar"))
        delegate = self
        allowsUserCustomization = true
        autosavesConfiguration = true
    }
}

extension MainWindowToolbar: NSToolbarDelegate {
    func toolbar(_ toolbar: NSToolbar, itemForItemIdentifier itemIdentifier: NSToolbarItem.Identifier, willBeInsertedIntoToolbar flag: Bool) -> NSToolbarItem? {
        nil
    }

    func toolbarAllowedItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        [
            .flexibleSpace,
            .separator,
            .space,
            .toggleSidebar,
            .print,
            .cloudSharing,
            .showColors,
            .showFonts,
        ]
    }

    func toolbarDefaultItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        [.showColors, .showFonts, .flexibleSpace, .print]
    }
}

