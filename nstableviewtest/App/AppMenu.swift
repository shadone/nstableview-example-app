//
//  AppMenu.swift
//  nstableviewtest
//
//  Created by Denis Dzyubenko on 06/08/2020.
//  Copyright © 2020 Denis Dzyubenko. All rights reserved.
//

import Cocoa

// by Ryan Theodore The (https://medium.com/@theboi)
// from https://medium.com/@theboi/macos-apps-without-storyboard-or-xib-menu-bar-in-swift-5-menubar-and-toolbar-6f6f2fa39ccb

private extension NSMenuItem {
    convenience init(title string: String,
                     target: AnyObject = self as AnyObject,
                     action selector: Selector?,
                     keyEquivalent charCode: String,
                     modifier: NSEvent.ModifierFlags = .command) {
        self.init(title: string, action: selector, keyEquivalent: charCode)
        keyEquivalentModifierMask = modifier
        self.target = target
    }

    convenience init(title string: String, submenuItems: [NSMenuItem]) {
        self.init(title: string, action: nil, keyEquivalent: "")
        self.submenu = NSMenu()
        self.submenu?.items = submenuItems
    }

    convenience init(title string: String, submenu: NSMenu) {
        self.init(title: string, action: nil, keyEquivalent: "")
        self.submenu = submenu
    }
}


class AppMenu: NSMenu {
    // MARK: Public

    let servicesMenu = NSMenu(title: "Services")
    let windowsMenu = NSMenu(title: "Window")
    let helpMenu = NSMenu(title: "Help")

    // MARK: Private

    private let applicationName = ProcessInfo.processInfo.processName

    lazy private var appMenu: NSMenuItem = {
        let menu = NSMenuItem()
        menu.submenu = NSMenu(title: "appMenu")
        menu.submenu?.items = [
            NSMenuItem(
                title: "About \(applicationName)",
                action: #selector(NSApplication.orderFrontStandardAboutPanel(_:)),
                keyEquivalent: ""),

            NSMenuItem.separator(),

            NSMenuItem(title: "Preferences…", action: nil, keyEquivalent: ","),

            NSMenuItem.separator(),

            NSMenuItem(title: "Services", submenu: servicesMenu),

            .separator(),

            NSMenuItem(
                title: "Hide \(applicationName)",
                action: #selector(NSApplication.hide(_:)),
                keyEquivalent: "h"),
            NSMenuItem(
                title: "Hide Others",
                target: self,
                action: #selector(NSApplication.hideOtherApplications(_:)),
                keyEquivalent: "h",
                modifier: .init(arrayLiteral: [.command, .option])),
            NSMenuItem(
                title: "Show All",
                action: #selector(NSApplication.unhideAllApplications(_:)),
                keyEquivalent: ""),

            NSMenuItem.separator(),

            NSMenuItem(
                title: "Quit \(applicationName)",
                action: #selector(NSApplication.terminate(_:)),
                keyEquivalent: "q"),
        ]
        return menu
    }()

    lazy private var fileMenuItem: NSMenuItem = {
        let menu = NSMenuItem()
        menu.submenu = NSMenu(title: "File")
        menu.submenu?.items = [
            NSMenuItem(
                title: "New Tab",
                action: #selector(AppDelegate.newTab(_:)),
                keyEquivalent: "t"),
            NSMenuItem(
                title: "Close Window",
                action: #selector(NSWindow.performClose(_:)),
                keyEquivalent: "w"),
        ]
        return menu
    }()

    lazy private var editMenuItem: NSMenuItem = {
        let menu = NSMenuItem()
        menu.submenu = NSMenu(title: "Edit")
        menu.submenu?.items = [
            NSMenuItem(
                title: "Undo",
                action: #selector(UndoManager.undo),
                keyEquivalent: "z"),
            NSMenuItem(
                title: "Redo",
                action: #selector(UndoManager.redo),
                keyEquivalent: "Z"),

            NSMenuItem.separator(),

            NSMenuItem(
                title: "Cut",
                action: #selector(NSText.cut(_:)),
                keyEquivalent: "x"),
            NSMenuItem(
                title: "Copy",
                action: #selector(NSText.copy(_:)),
                keyEquivalent: "c"),
            NSMenuItem(
                title: "Paste",
                action: #selector(NSText.paste(_:)),
                keyEquivalent: "v"),

            NSMenuItem.separator(),

            NSMenuItem(
                title: "Select All",
                action: #selector(NSText.selectAll(_:)),
                keyEquivalent: "a"),
        ]
        return menu
    }()

    lazy private var viewMenuItem: NSMenuItem = {
        let menu = NSMenuItem()
        menu.submenu = NSMenu(title: "View")
        menu.submenu?.items = [
            NSMenuItem(
                title: "Show Toolbar",
                action: #selector(NSWindow.toggleToolbarShown(_:)),
                keyEquivalent: "t",
                modifier: [.command, .option]),
            NSMenuItem(
                title: "Customize Toolbar…",
                action: #selector(NSWindow.runToolbarCustomizationPalette(_:)),
                keyEquivalent: ""),

            NSMenuItem.separator(),

            NSMenuItem(
                title: "Toggle Sidebar",
                action: #selector(NSSplitViewController.toggleSidebar(_:)),
                keyEquivalent: "s",
                modifier: [.command, .option]),

            NSMenuItem.separator(),

            // Enter Fullscreen will be added at the end of this menu by macOS
        ]
        return menu
    }()

    lazy private var windowMenuItem: NSMenuItem = {
        let menu = NSMenuItem()
        menu.submenu = windowsMenu

        menu.submenu?.insertItem(
            withTitle: "Minimize",
            action: #selector(NSWindow.miniaturize(_:)),
            keyEquivalent: "m",
            at: 0)
        menu.submenu?.insertItem(
            withTitle: "Zoom",
            action: #selector(NSWindow.performZoom(_:)),
            keyEquivalent: "",
            at: 1)

        menu.submenu?.addItem(.separator())

        menu.submenu?.addItem(
            withTitle: "Bring All to Front",
            action: #selector(NSApplication.arrangeInFront(_:)),
            keyEquivalent: "")

        return menu
    }()

    lazy private var helpMenuItem: NSMenuItem = {
        let menu = NSMenuItem()
        menu.submenu = helpMenu
        return menu
    }()

    // MARK: Functions

    override init(title: String) {
        super.init(title: title)

        items = [
            appMenu,
            fileMenuItem,
            editMenuItem,
            viewMenuItem,
            windowMenuItem,
            helpMenuItem,
        ]
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
