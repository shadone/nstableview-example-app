//
//  main.swift
//  nstableviewtest
//
//  Created by Denis Dzyubenko on 06/08/2020.
//  Copyright © 2020 Denis Dzyubenko. All rights reserved.
//

import Cocoa

let appDelegate = AppDelegate()
NSApplication.shared.delegate = appDelegate

let appMenu = AppMenu()
NSApplication.shared.registerServicesMenuSendTypes([.string], returnTypes: [])
NSApplication.shared.menu = appMenu
NSApplication.shared.servicesMenu = appMenu.servicesMenu
NSApplication.shared.helpMenu = appMenu.helpMenu
NSApplication.shared.windowsMenu = appMenu.windowsMenu

_ = NSApplicationMain(CommandLine.argc, CommandLine.unsafeArgv)
