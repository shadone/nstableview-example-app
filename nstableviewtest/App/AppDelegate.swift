//
//  AppDelegate.swift
//  nstableviewtest
//
//  Created by Denis Dzyubenko on 06/08/2020.
//  Copyright © 2020 Denis Dzyubenko. All rights reserved.
//

import Cocoa

class AppDelegate: NSObject, NSApplicationDelegate {
    private var window: NSWindow?

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        let windowController = MainWindowController()
        window = windowController.window
        window?.makeKeyAndOrderFront(self)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    @objc func newTab(_ sender: Any?) {
        let currentKeyWindow = NSApplication.shared.keyWindow

        let newWindowController = MainWindowController()
        let newWindow = newWindowController.window!

        if let window = currentKeyWindow {
            // add tab to existing window
            window.addTabbedWindow(newWindow, ordered: .above)
            window.selectNextTab(self)
        } else {
            newWindowController.showWindow(self)
        }
    }
}

